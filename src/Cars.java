import java.util.Random;

public class Cars {
    Random random = new Random();
    MakingMoves makingMoves = new MakingMoves();

    public void newTurn (int[] type, int[] parking, int[] turn, int seats) {
        if (amountOfPassengerCarsGenerate(seats) + amountOfTrucksGenerate(seats) <= freePlaces(parking)) {
            for (int n = 0; n < parking.length; n++) {
                if (parking[n] == 0) {
                    for (int j = 0; j < amountOfPassengerCarsGenerate(seats); j++) {
                        if (type[n] == 0) {
                            parking[n] = carNumberGenerate();
                            turn[n] = movesGenerate(seats);
                        }
                    }
                }
            }
            for (int n = 0; n < parking.length; n++) {
                if (parking[n] == 0) {
                    for (int j = 0; j < amountOfTrucksGenerate(seats); j++) {
                        if (type[n] == 1) {
                            parking[n] = carNumberGenerate();
                            turn[n] = movesGenerate(seats);
                        }
                        else if (type[n] == 0 && type[n + 1] == 0) {
                            parking[n] = carNumberGenerate();
                            parking[n + 1] = parking[n];
                            turn[n] = movesGenerate(seats);
                            turn[n + 1] = turn[n];
                        }
                    }
                }
            }
        }
        else System.out.println("The parking lot is full. Wait in line.");

        for (int i = 0; i < turn.length; i++) {
            if (turn[i] != 0) {
                turn[i] = turn[i] - 1;
            }
            if (turn[i] == 0) {
                parking[i] = 0;
            }
        }

        print(type, parking, turn);

        makingMoves.menu(type, parking, turn, seats);
    }

    public void print (int[] type, int[] parking, int[] turn) {
        System.out.print("Parking type: ");
        for (int j : type) {
            System.out.print(j + " ");
        }
        System.out.println();
        System.out.print("Parking numbers: ");
        for (int j : parking) {
            System.out.print(j + " ");
        }
        System.out.println();
        System.out.print("Turns: ");
        for (int j : turn) {
            System.out.print(j + " ");
        }
        System.out.println();
    }


    public int freePlaces (int[] parking) {
        int freePlaces = 0;
        for (int i = 0; i < parking.length; i++) {
            if (parking[i] == 0) {
                freePlaces++;
            }
        }
       return freePlaces;
    }

    public int carNumberGenerate () {
        int min = 1000;
        int max = 9999;
        int diff = max - min;
        return random.nextInt(diff + 1) + min;
    }

    public int amountOfPassengerCarsGenerate (int seats) {
        return random.nextInt(seats/3);
    }

    public int amountOfTrucksGenerate (int seats) {
        return random.nextInt(seats/3);
    }

    public int movesGenerate (int seats) {
        int min = 1;
        int diff = seats - min;
        return random.nextInt(diff + 1) + min;
    }

    public int amountPassenger (int[] type) {
        int amountPassenger = 0;
        for (int i = 0; i < type.length; i++) {
            if (type[i] == 0) {
                amountPassenger++;
            }
        }
        return amountPassenger;
    }

    public int amountTrucks (int[] type) {
        int amountTrucks = 0;
        for (int i = 0; i < type.length; i++) {
            if (type[i] == 0) {
                amountTrucks++;
            }
        }
        return amountTrucks;
    }

}
