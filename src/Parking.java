import java.util.Random;

public class Parking {
    Cars cars = new Cars();
    MakingMoves makingMoves = new MakingMoves();

    public void cleanParking (int[] type, int[] parking, int[] turn) {
        for (int i = 0; i < parking.length; i++) {
            type[i] = 0;
            parking[i] = 0;
            turn[i] = 0;
        }
        System.out.println("Parking is clear.");
        cars.print(type, parking, turn);
    }

    public void checkingPlaces (int[] type, int[] parking,int[] turn, int seats) {
        System.out.println("Total free parking spaces: " + cars.freePlaces(parking));
        System.out.println("Total occupied parking spaces: " + (type.length - cars.freePlaces(parking)));
        System.out.println("Places for a passenger car: " + cars.amountPassenger(type));
        System.out.println("Places for a truck: " + cars.amountTrucks(type));
        makingMoves.menu(type, parking, turn, seats);
    }

}
