import java.util.Random;
import java.util.Scanner;

public class MakingMoves {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Random random = new Random();
        Cars cars = new Cars();
        MakingMoves makingMoves = new MakingMoves();
        System.out.println("How many parking spaces?");
        int seats = scanner.nextInt();
        int[] type = new int[seats];
        int[] parking = new int[seats];
        int[] turn = new int[seats];
        int rnd;
        for (int i = 0; i < type.length; i++) {
            rnd = random.nextInt(2);
            type [i] = rnd;
        }
        cars.print(type, parking, turn);

        System.out.println("Places in parking type with the number 0 - for passenger cars; places with the number 1 - for trucks.");
        System.out.println();
        System.out.println("Parking lot is set up.");
        System.out.println();
        makingMoves.menu(type, parking, turn, seats);
    }

    public void menu (int[] type, int[] parking, int[] turn, int seats) {
        Scanner scanner = new Scanner(System.in);
        Cars cars = new Cars();
        Parking parkingClass = new Parking();
        System.out.println("Which will you choose?");
        System.out.println("1. Complete the turn and go to the next");
        System.out.println("2. Find out how many places are occupied, how many are free and how many of places for passenger cars and trucks");
        System.out.println("3. Clear the parking lot of all cars");
        int answer = scanner.nextInt();
        switch (answer) {
            case 1:
                cars.newTurn(type, parking, turn, seats);
                break;
            case 2:
                parkingClass.checkingPlaces(type, parking,turn,seats);
                break;
            case 3:
                parkingClass.cleanParking(type, parking, turn);
                break;
        }
    }
}
